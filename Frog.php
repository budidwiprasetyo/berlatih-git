<?php
    
    require_once('Animal.php');
    class Frog extends Animal{
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";
        
        public function jump(){
            echo "Hop Hop";
        }
    }


?>